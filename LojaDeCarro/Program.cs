﻿using System;

namespace LojaDeCarro
{
    class Program
    {
        static void Main(string[] args)
        {
            Carro carro = new Carro();

            carro.Marca = "Renault";
            carro.Modelo = "Sandeiro";
            carro.Preco = 49000;
            carro.Km = 0;
            carro.Ano = 2017;
            carro.NumeroDonos = 1;

            double valorParcela = carro.ValorParcela(20);
            Console.WriteLine(carro.Marca);
            Console.WriteLine(carro.Modelo);
            Console.WriteLine(carro.Preco);
            Console.WriteLine(carro.Km);
            Console.WriteLine(carro.Ano);
            Console.WriteLine(carro.NumeroDonos);
            Console.WriteLine(carro.Km);

            double valorParcela = carro.ValorParcela(20);
            string tipoCarro = carro.TipoCarro();
            Console.WriteLine(valorParcela);
            Console.WriteLine(tipoCarro);


        }
    }
}
